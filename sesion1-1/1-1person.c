#include <stdio.h> 
#include <string.h>
struct _Person {
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name 
typedef struct _Person Person; int main(int argc, char* argv[]) {
    Person Peter;
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175;
    // Assign the weight
    Peter.weightkg = 80.5;
    // Show the information of the Peter data structure on the screen
    printf("Name: %s\n", Peter.name);
    printf("Height: %d\n", Peter.heightcm);
    printf("Weight: %f", Peter.weightkg);
    return 0;
}
