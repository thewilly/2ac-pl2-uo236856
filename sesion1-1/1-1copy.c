#include <stdio.h>
#include <string.h>

int copy(char * source, char * destination, unsigned int lengthDestination) {
	 while(*source != '\0' && lengthDestination != 0) {
		*destination = *source++;
		++destination;
		lengthDestination = lengthDestination - 1;
}
	return 0;
}

int main() {
	char origin[6] = "123456";
	char destination[4];
	copy(origin, destination, 4);
	printf("Contenido de destino: %s\n", destination);
}
